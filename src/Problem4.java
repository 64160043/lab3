import java.util.Scanner;

public class Problem4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int Number;
        int sum = 0;
        int count = 0;

        do {
            System.out.print("Please input Number: ");
            Number = sc.nextInt();
            if (Number != 0) {
                sum = sum + Number;
                count++;
                System.out.println("Sum: " + sum + "Avg: " + (((double) sum) / count));

            }

        } while (Number != 0);

        System.out.println("Bye");

        // Number = sc.nextInt();

        sc.close();
    }

}
